@echo off
::create the Word Autorecover folder on the users desktop
if not exist "C:\Users\ExamUserNoSpellCheck\Desktop\Autorecover" mkdir "C:\Users\ExamUserNoSpellCheck\Desktop\Autorecover"
if not exist "C:\Users\ExamUserSpellCheck\Desktop\Autorecover" mkdir "C:\Users\ExamUserSpellCheck\Desktop\Autorecover"

::Copy Microsoft Word Links into the Users Desktop, check for OS and word versions
:CheckOS
IF EXIST "%PROGRAMFILES(X86)%" (GOTO 64BIT) ELSE (GOTO 32BIT)
:64BIT
echo 64-bit...
IF EXIST "%PROGRAMFILES(X86)%\Microsoft Office\Office12" (GOTO 64BIT2007) ELSE (GOTO 64BIT2010)
GOTO END
:64BIT2007
echo Office 2007 32-Bit, copying shortcut...
copy "%~dp0Microsoft Word 2007 ProgramFilesX86.lnk" "%USERPROFILE%\Desktop\Microsoft Word 2007.lnk" 
GOTO END
:64BIT2010
echo Office 2010 32-Bit, copying shortcut...
copy "%~dp0Microsoft Word 2010 ProgramFilesX86.lnk" "%USERPROFILE%\Desktop\Microsoft Word 2010.lnk" 
GOTO END
:32BIT
IF EXIST "%PROGRAMFILES%\Microsoft Office\Office14" (GOTO 32BIT2007) ELSE (GOTO 32BIT2010)
GOTO END
:32BIT2007
echo Office 2007 32-Bit, copying shortcut...
copy "%~dp0Microsoft Word 2007 ProgramFiles.lnk" "%USERPROFILE%\Desktop\Microsoft Word 2010.lnk" 
GOTO END
:32BIT2010
echo Office 2010 32-Bit, copying shortcut...
copy "%~dp0Microsoft Word 2010 ProgramFiles.lnk" "%USERPROFILE%\Desktop\Microsoft Word 2010.lnk" 
GOTO END
:END

copy "c:\examscripts\Delete Exam Data.lnk" "%USERPROFILE%\Desktop\" /Y

::call the vbs script to pin Microsoft Word to the Start Menu
cscript c:\examscripts\PinWordToStartMenu.vbs