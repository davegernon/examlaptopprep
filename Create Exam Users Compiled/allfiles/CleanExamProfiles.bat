@echo off
:: Last Edited 22/05/2015 by Dave Gernon, Keble College
::
:: This script gives the user a choice of deleting all exam user accounts or exiting.
::
:: If delete is selected it then adds the DeleteExamUserAndReCreate.bat script to the
:: HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce key where it is run at next startup

:home
cls
echo.
echo Please CAREFULLY make your selection:
echo =============
echo.
echo 1) Delete ALL Exam User Data for BOTH Exam Accounts
echo.
echo 5) Exit
echo.
set /p option=Type option:  
if "%option%"=="1" goto DELETEDATA
if "%option%"=="5" goto end
echo Please select either option 1 or option 5
goto home

:DELETEDATA
del "c:\examscripts\deleteuserscheck\%computername%"  /S /Q
shutdown /r /t 1

:end