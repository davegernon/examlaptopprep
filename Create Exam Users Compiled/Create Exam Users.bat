@echo off
:: This script checks the architecture of the system and calls the relevant installation script
::
:: Last Edited 20/05/2015
:: Created by Dave Gernon, Keble College
::
::	Thanks goes to http://www.lukecjdavis.com/remove-spelling-and-grammar-from-ms-word/ , https://technet.microsoft.com/en-us/library/cc179143(office.12).aspx#section1,  Microsoft TechNet Channels and random Google searches!
::	
:: You're welcome to reuse this script as you see fit
::
:: This Script is designed for Windows 7 32-bit or 64-bit and Microsoft Office 2007 or 2010 installation
::
:: It completes the following tasks:
:: 	1. Create two users in Windows named ExamUserNoSpellCheck (password protected) and 
::	   ExamUserSpellCheck (no password)
::
::	2. Copy Group Policy files for each user into C:\Windows\System32\GroupPolicyUsers
::
::	3. Create a directory c:\examscripts
::
::	4. Populate C:\examscripts with:
::		a. Microsoft Office Word shortcuts for each platform
::		b. logon.bat script 
::		c. PinWordToStartMenu.vbs
::		d. blue.jpg
::		e. grey.jpg
::
::	5. Copy the Microsoft Office 2007 and 2010 ADMX and ADML files to C:\Windows\PolicyDefinitions 
::	   and C:\Windows\PolicyDefinitions\en-US folders respectively
::
::	4b: logon.bat file:
::		Creates the folder "%CURRENTUSER%\Desktop\Autorecover" folder if it doesn't exist
::		Copies the relevant Microsoft Office Word shortcut from c:\examscripts to "%CURRENTUSER%\Desktop"
::		Calls the 4c script PinWordToStartMenu.vbs
::
::	4c: PinWordToStartMenu.vbs:
::		Places a shortcut on the %CURRENTUSER% Windows Start Menu for the installed version of office, trawling the 
::		"Start Menu\All Programs\Microsoft Office\" folder for the shortcut.
::
::	Group Policy:
::		There are two group policies deployed through this script, one which has the spell checker & grammar 
::		checker disabled for Word and one which doesn't.
::		Both scripts then:
::			1. Lock down the file system so the user cannot write anywhere but their own User directory
::			2. Remove balloon pop ups
::			3. Remove the system tray and clock
::			4. Trim the start menu down removing Programs, recently used programs and leaving
::			   Computer, Control Panel and Devices and Printers
::			5. The control panel is trimmed to just display:
::				Autoplay, Date & Time, Ease of Access Center, Fonts, Keyboard, Mouse and Region & Language
::			6. Removable media is blocked except for storage devices (USB Drives)
::			   NOTE: USB Ports should be physically blocked but accessible in case of emergency
::			7. Block office application from running except for MS Word
::	
::	DELETING THE USER ACCOUNTS:
::	1. Delete the user accounts through Windows ensuring the profile directories are removed
::	2. Delete the C:\Windows\System32\GroupPolicyUsers sub folders to remove the GPOs
::	3. Delete the C:\Windows\System32\GroupPolicy to remove the applied machine GPO
::	4. Delete the C:\examscripts directory
::	5. REBOOT
::
:CheckOS
IF EXIST "%PROGRAMFILES(X86)%" (GOTO 64BIT) ELSE (GOTO 32BIT)

:64BIT
"%CD%\ExamUsers_64bit.exe"
GOTO END

:32BIT
"%CD%\ExamUsers_32bit.exe"
GOTO END

:END