# Oxford Laptop Auto Configuration for Examinations #

Copyright: Keble College, Oxford University

Contributors: Dave Gernon

**Thanks go to:**

Frank P. Westlake (Recycle.exe),

DeploymentGuys at TechNet (Pinning items to Start Menu VBS).

Microsoft (Policy Files)

### DESCRIPTION ###

This Repo contains two directories, one for the open batch file and one with the compiled executable files alongside the source batch file.

* Scripts to prepare a laptop for exam conditions
* version : 1.0

### USAGE ###

* Download either of the two directories (versions) and launch the batch file or the executable as a local or domain administrator.

CONFIG:
* no configuration is required, however you're welcome to improve the batch file "CreateExamUsers.bat" or the executable. 

Executable changes require:
  1. Editing the ExamUsers.bat in the "Create Exam Users Compiled" directory
  2. Then using BAT2EXE (http://www.f2ko.de/en/b2e.php ) to generate an x86 and x64 version of ExamUsers.bat including all files from the "allfiles" directory.
  3. Then add both of those executables as component files of the last executable, using the "Create Exam Users.bat" file as the source for the BAT2EXE program.

### SOURCE ###
* The directories contain their own dependent files, these are the source of the batch scripts.

### DEPLOYMENT ###
Copy the directory you'd like to use (either compiled as exe or raw batch) and run the exe or the batch from the directory on the target machione