@echo off
:: Last Edited 22/05/2015 by Dave Gernon
:: Created by Dave Gernon, Keble College
::
::
::	Thanks goes to http://www.lukecjdavis.com/remove-spelling-and-grammar-from-ms-word/ , https://technet.microsoft.com/en-us/library/cc179143(office.12).aspx#section1,  Microsoft TechNet Channels and random Google searches!
::	
:: You're welcome to reuse this script as you see fit
::
:: This Script is designed for Windows 7 32-bit or 64-bit and Microsoft Office 2007 or 2010 installation
::
::	******* REQUIREMENTS *********
::	Windows 7 x86 or x64 
::	Microsoft Office 2007 or 2010
::
:: Directories required to be included in this batch file directory:
::	allfiles
::	examscripts
::	GroupPolicyComputer
::	GroupPolicyUsers
::	OfficePolicyFiles
::	OfficeShortcuts
::	RecycleEXE



:: It completes the following tasks:
:: 	1. Create two users in Windows named ExamUserNoSpellCheck (password protected ******AND EXPOSED IN THIS BATCH FILE SO REPLACE WITH YOUR OWN WHERE YOU SEE THE STRING CHANGEME*****) and 
::	   ExamUserSpellCheck (no password)
::
::	2. Copy Group Policy files for each user into C:\Windows\System32\GroupPolicyUsers
::
::	3. Create a directory c:\examscripts
::
::	4. Populate C:\examscripts with:
::		a. Microsoft Office Word shortcuts for each platform
::		b. logon.bat script 
::		c. PinWordToStartMenu.vbs
::		d. blue.jpg
::		e. grey.jpg
::
::	5. Copy the Microsoft Office 2007 and 2010 ADMX and ADML files to C:\Windows\PolicyDefinitions 
::	   and C:\Windows\PolicyDefinitions\en-US folders respectively
::
::	4b: logon.bat file:
::		Creates the folder "%CURRENTUSER%\Desktop\Autorecover" folder if it doesn't exist
::		Copies the relevant Microsoft Office Word shortcut from c:\examscripts to "%CURRENTUSER%\Desktop"
::		Calls the 4c script PinWordToStartMenu.vbs
::
::	4c: PinWordToStartMenu.vbs:
::		Places a shortcut on the %CURRENTUSER% Windows Start Menu for the installed version of office, trawling the 
::		"Start Menu\All Programs\Microsoft Office\" folder for the shortcut.
::
::	Group Policy:
::		There are three group policies deployed through this script, one which has the spell checker
::		 & grammar checker disabled for Word and one which doesn't. The third is the computer
::		 (machine) GPO.
::
::		The machine policy adds a Computer Shutdown Script pointing to
::		 c:\examscript\createusers\DeleteExamUserAndReCreate.bat which in turn checks for the file
::		 c:\examscripts\deleteuserscheck\%computername% and if not present it executes else it does
::		 nothing.
::		It also disables sleep for the computer.
::
::		Both user policies then:
::			1. Lock down the file system so the user cannot write anywhere but their own User directory
::			2. Remove balloon pop ups
::			3. Remove the system tray and clock
::			4. Trim the start menu down removing Programs, recently used programs and leaving
::			   Computer, Control Panel and Devices and Printers
::			5. The control panel is trimmed to just display:
::				Autoplay, Date & Time, Ease of Access Center, Fonts, Keyboard, Mouse and Region & Language
::			6. Removable media is blocked except for storage devices (USB Drives)
::			   NOTE: USB Ports should be physically blocked but accessible in case of emergency
::			7. Block office application from running except for MS Word
::			8. Disables the screensaver
::	
::
::			********************************BEGIN SCRIPT********************************
::
::			********************************BEGIN SCRIPT********************************
:home
cls
echo. :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
echo  ::               Oxford Exam User Account Creation                 ::
echo  ::                                                                 ::
echo  ::  This script will create two exam users in Windows 7 x86 / x64  ::
echo  ::  Group Policies will then be applied to both user accounts and  ::
echo  ::  over-write the machine group policy for the computer to enable ::
echo  ::  the shutdown script to be installed. The shutdown script will  ::
echo  ::  provide a facility, by means of a checker file the exam users  ::
echo  ::  can delete, to wipe the user accounts and re-create them.      ::
echo  ::                                                                 ::
echo  ::  Executing this script will force a system reboot when complete ::
echo  ::                                                                 ::
echo  :: Installation log file at c:\examscripts\INSTALL.log             ::
echo  ::                                                                 ::
echo  :: NOTE: Please ENSURE you've edited the CHANGEME password for the ::
echo  :: ExamUserSpellCheck account in this script (line 110)            ::
echo  :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
echo.
echo =============
echo.
echo 1) Proceed
echo.
echo 5) Exit
echo.
set /p option=Type option:  
if "%option%"=="1" goto execute
if "%option%"=="5" goto end
echo Please select either option 1 or option 5
goto home


:createusers
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::: Group Policy Section :::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::create the two exam users, one destined to have spell check enabled, check they don't exist before creating
echo [TASK 2 of 17] Creating the ExamUserNoSpellCheck user...
net user | find /i "ExamUserNoSpellCheck" || net user /ADD "ExamUserNoSpellCheck" /expires:NEVER /fullname:"Exam User Spellcheck Disabled" /passwordreq:NO /PASSWORDCHG:No
echo.

echo [TASK 3 of 17] Creating the ExamUserSpellCheck user...
net user | find /i "ExamUserSpellCheck" || net user /ADD "ExamUserSpellCheck" CHANGEME /expires:NEVER /fullname:"Exam User Spellcheck Enabled" /passwordreq:YES /PASSWORDCHG:No
echo.

::get the SID's of the two exam users and assign them to %ExamUserNoSpellCheck% and %ExamUserSpellCheck%
echo [TASK 4 of 17] Assigning ExamUserNoSpellCheck SID variable...
FOR /F "tokens=1,2 delims==" %%s IN ('wmic path win32_useraccount where name^='ExamUserNoSpellCheck' get sid /value ^| find /i "SID"') DO SET ExamUserNoSpellCheck=%%t

echo [TASK 5 of 17] Assigning ExamUserSpellCheck SID variable...
FOR /F "tokens=1,2 delims==" %%s IN ('wmic path win32_useraccount where name^='ExamUserSpellCheck' get sid /value ^| find /i "SID"') DO SET ExamUserSpellCheck=%%t
echo.

:: copy the machine group policy, this calls the shutdown script to clean the machine
echo [TASK 6 of 17] copying the computer group policy...
xcopy "%~dp0GroupPolicyComputer" "%WINDIR%\System32\GroupPolicy" /E /H /Y
echo.

::Copy the Group Policy User Files
echo [TASK 7 of 17] Copying the GroupPolicyUsers Folders and Files...
xcopy "%~dp0GroupPolicyUsers" "%WINDIR%\System32\GroupPolicyUsers" /E /H /Y
echo.


:: rename the group policy folders to match the SIDs of the two new accounts
echo [TASK 8 of 17] renaming both new GroupPolicyUsers folders to match the SIDs of the two new accounts...
if exist "%WINDIR%\System32\GroupPolicyUsers\ExamUserNoSpellCheck\%ExamUserNoSpellCheck%" rmdir "%WINDIR%\System32\GroupPolicyUsers\ExamUserNoSpellCheck\%ExamUserNoSpellCheck%" /S /Q
if exist "%WINDIR%\System32\GroupPolicyUsers\ExamUserNoSpellCheck\%ExamUserSpellCheck%" rmdir "%WINDIR%\System32\GroupPolicyUsers\ExamUserNoSpellCheck\%ExamUserSpellCheck%" /S /Q
rename "%WINDIR%\System32\GroupPolicyUsers\ExamUserNoSpellCheck\" %ExamUserNoSpellCheck%
rename "%WINDIR%\System32\GroupPolicyUsers\ExamUserSpellCheck\" %ExamUserSpellCheck%
echo.

::apply permissions to new group policy folders:
::first the GroupPolicyUsers
echo [TASK 8 of 17] applying security permissions to the GroupPolicyUsers folder...
icacls "%WINDIR%\System32\GroupPolicyUsers" /reset /T 
icacls "%WINDIR%\System32\GroupPolicyUsers"  /inheritance:d  
icacls "%WINDIR%\System32\GroupPolicyUsers" /grant "NT AUTHORITY\Authenticated Users":(RX)  /grant "NT AUTHORITY\Authenticated Users":(OI)(CI)(IO)(GR,GE) /grant Administrators:(OI)(CI)(IO)(F) /grant Administrators:(F) /Grant SYSTEM:F /grant SYSTEM:(OI)(CI)(IO)(F) /Q /T /C  
echo.

::Now the GroupPolicyUsers Sub Folder %ExamUserNoSpellCheck%
echo [TASK 9 of 17] applying security permissions to the GroupPolicyUsers sub-folders and files...
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserNoSpellCheck%" /reset /T 
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserNoSpellCheck%"  /inheritance:d /remove "Authenticated Users" /T /Q /C 
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserNoSpellCheck%" /grant ExamUserNoSpellCheck:(RX) /grant ExamUserNoSpellCheck:(OI)(CI)(IO)(GR,GE) /grant Administrators:(F) /grant Administrators:(OI)(CI)(IO)F /Grant SYSTEM:(F) /Grant SYSTEM:(OI)(CI)(IO)F  /Q /T /C 

::Now the GroupPolicyUsers Sub Folder %ExamUserSpellCheck%
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserSpellCheck%" /reset /T 
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserSpellCheck%"  /inheritance:d /remove "Authenticated Users" /T /Q /C 
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserSpellCheck%"  /grant ExamUserSpellCheck:(RX) /grant ExamUserSpellCheck:(OI)(CI)(IO)(GR,GE) /grant Administrators:(F) /grant Administrators:(OI)(CI)(IO)F /Grant SYSTEM:(F) /Grant SYSTEM:(OI)(CI)(IO)F  /Q /T /C 
echo.

::Hide the new GroupPolicyUsers folders
echo [TASK 10 of 17] hiding the new policy folders...
ATTRIB +H "%WINDIR%\System32\GroupPolicyUsers\%ExamUserNoSpellCheck%" /S /D 
ATTRIB +H "%WINDIR%\System32\GroupPolicyUsers\%ExamUserSpellCheck%" /S /D 
echo.

::copy the office 2007 and 2010 admx policy files to the computer
echo [TASK 11 of 17] copying the 56 Microsoft Office admx policy files...
xcopy /S /Y "%~dp0OfficePolicyFiles" "%WINDIR%\PolicyDefinitions"
echo.

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::: Logon Section ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

::copy the logon script which is called via gpo to create autorecover directories on users desktops
echo [TASK 12 of 17]  copying the logon.bat script and vbs script to  C:\examscripts...
xcopy "%~dp0examscripts" "C:\examscripts\*" /E /H /Y
echo.

::copy the word shortcuts 
echo [TASK 13 of 17] copying the word shortcuts ...
xcopy "%~dp0OfficeShortcuts\*.*" "C:\examscripts\*.*" /E /H /Y
echo.

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::: Data Wiping Section ::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Now begins the user cleaning files

::copy this entire installation directory and files into c:\examscript\createusers
echo [TASK 14 of 17] creating "c:\examscripts\createusers" and assigning permissions...
if not exist "c:\examscripts\createusers" md "c:\examscripts\createusers" 

::prevent the users viewing this folder, it's used by SYSTEM Scheduled Task only
icacls "c:\examscripts\createusers"  /deny ExamUserSpellCheck:(F) /deny ExamUserSpellCheck:(OI)(CI)(IO)F    /deny ExamUserNoSpellCheck:(F) /deny ExamUserNoSpellCheck:(OI)(CI)(IO)F  /grant Administrators:(F) /grant Administrators:(OI)(CI)(IO)F /Grant SYSTEM:(F) /Grant SYSTEM:(OI)(CI)(IO)F  /Q /T /C 
echo.

::copy all installation files into the restricted directory
echo [TASK 15 of 17] copying all scripts and installation files to "c:\examscripts\createusers"....
xcopy /S /Y /E /H "%~dp0allfiles" "c:\examscripts\createusers\*"
echo.

::coy the users CleanExamProfiles.exe and shortcut to the user readable directory
echo [TASK 16 of 17] copying the user executable and associated shortcut to "%CD%\CleanExamProfiles.exe"
copy "%~dp0examscripts\CleanExamProfiles.exe" "c:\examscripts" /Y
copy "%~dp0examscripts\Delete Exam Data.shortcut" "c:\examscripts\Delete Exam Data.lnk" /Y
echo.

:: create checking directory and file for scheduled task to reference
echo [TASK 17 of 17] creating the DeleteExamUserAndReCreate.bat script checking directory, populating with the checker file and assigning permissions...
md "c:\examscripts\deleteuserscheck"
echo.
:: create the check file that the scheduled task looks for to skip deleting users
if not exist "c:\examscripts\deleteuserscheck\%computername%" fsutil file createnew "c:\examscripts\deleteuserscheck\%computername%" 1

:: assign user rights to check dir and file
icacls "c:\examscripts\deleteuserscheck"  /grant ExamUserSpellCheck:(F) /grant ExamUserSpellCheck:(OI)(CI)(IO)F    /grant ExamUserNoSpellCheck:(F) /grant ExamUserNoSpellCheck:(OI)(CI)(IO)F  /grant Administrators:(F) /grant Administrators:(OI)(CI)(IO)F /Grant SYSTEM:(F) /Grant SYSTEM:(OI)(CI)(IO)F  /Q /T /C 
echo.

exit /b

:execute
::create the base directory for scripts and files
echo Completing setup and writing to "C:\examscripts\INSTALL.log"...
if not exist "C:\examscripts" md "C:\examscripts" 
echo.

call :createusers>"C:\examscripts\INSTALL.log"  

goto finishup

:finishup
echo The Computer will now restart....
echo.
pause
shutdown /f /r /t 1
::
:end