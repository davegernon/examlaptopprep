On Error resume next

Const CSIDL_COMMON_PROGRAMS = &H17 
Const CSIDL_PROGRAMS = &H2 


Set objShell = CreateObject("Shell.Application") 
Set objAllUsersProgramsFolder = objShell.NameSpace(CSIDL_COMMON_PROGRAMS) 
strAllUsersProgramsPath = objAllUsersProgramsFolder.Self.Path 
Set objFolder = objShell.Namespace(strAllUsersProgramsPath & "\Microsoft Office") 
Set objFolderItem = objFolder.ParseName("Microsoft Office Word 2007.lnk") 
Set colVerbs = objFolderItem.Verbs 
For Each objVerb in colVerbs 
    If Replace(objVerb.name, "&", "") = "Pin to Start Menu" Then objVerb.DoIt 
Next

Set objShell = CreateObject("Shell.Application") 
Set objAllUsersProgramsFolder = objShell.NameSpace(CSIDL_COMMON_PROGRAMS) 
strAllUsersProgramsPath = objAllUsersProgramsFolder.Self.Path 
Set objFolder = objShell.Namespace(strAllUsersProgramsPath & "\Microsoft Office") 
Set objFolderItem = objFolder.ParseName("Microsoft Word 2010.lnk") 
Set colVerbs = objFolderItem.Verbs 
For Each objVerb in colVerbs 
    If Replace(objVerb.name, "&", "") = "Pin to Start Menu" Then objVerb.DoIt 
Next

