@echo off
:: Last Edited 22/05/2015 by Dave Gernon, Keble College
::
:: This script deletes the ExamUserSpellCheck and ExamUserNoSpellCheck user profiles
:: It also deletes the recycler.
:: It then deletes the user directories and calls the ExamUsers.bat script to recreate them
::
:: THIS IS A TRIMMED VERSION OF THE ExamUsers.bat script to be run after a profile clean is initiated by the user
::
::	Thanks goes to http://www.lukecjdavis.com/remove-spelling-and-grammar-from-ms-word/ , https://technet.microsoft.com/en-us/library/cc179143(office.12).aspx#section1,  Microsoft TechNet Channels and random Google searches!
::	
:: You're welcome to reuse this script as you see fit
::
:: This Script is designed for Windows 7 32-bit or 64-bit and Microsoft Office 2007 or 2010 installation
::
:: It completes the following tasks:
:: 	1. Create two users in Windows named ExamUserNoSpellCheck (password protected) and 
::	   ExamUserSpellCheck (no password)
::
::	2. Copy Group Policy files for each user into C:\Windows\System32\GroupPolicyUsers
::
::	Group Policy:
::		There are two group policies deployed through this script, one which has the spell checker & grammar 
::		checker disabled for Word and one which doesn't.
::		Both scripts then:
::			1. Lock down the file system so the user cannot write anywhere but their own User directory
::			2. Remove balloon pop ups
::			3. Remove the system tray and clock
::			4. Trim the start menu down removing Programs, recently used programs and leaving
::			   Computer, Control Panel and Devices and Printers
::			5. The control panel is trimmed to just display:
::				Autoplay, Date & Time, Ease of Access Center, Fonts, Keyboard, Mouse and Region & Language
::			6. Removable media is blocked except for storage devices (USB Drives)
::			   NOTE: USB Ports should be physically blocked but accessible in case of emergency
::			7. Block office application from running except for MS Word
::	
::
::			********************************BEGIN SCRIPT********************************


if exist "c:\examscripts\deleteuserscheck\%computername%" goto end
goto execute 

:DELETE
::First delete the user accounts
net user ExamUserNoSpellCheck /del 
if exist "C:\Users\ExamUserNoSpellCheck" rd /s "C:\Users\ExamUserNoSpellCheck" /S /Q
net user ExamUserSpellCheck /del 
if exist "C:\Users\ExamUserSpellCheck" rd /s "C:\Users\ExamUserSpellCheck" /S /Q

::Now delete the recycler, thanks to http://ss64.net/westlake/xp/index.html
"C:\examscripts\createusers\recycle.exe" /E /F

::create the two exam users, one destined to have spell check enabled, check they don't exist before creating
net user /ADD "ExamUserNoSpellCheck" /expires:NEVER /fullname:"Exam User Spellcheck Disabled" /passwordreq:NO /PASSWORDCHG:No

net user /ADD "ExamUserSpellCheck" examinee /expires:NEVER /fullname:"Exam User Spellcheck Enabled" /passwordreq:YES /PASSWORDCHG:No

::get the SID's of the two exam users and assign them to %ExamUserNoSpellCheck% and %ExamUserSpellCheck%
FOR /F "tokens=1,2 delims==" %%s IN ('wmic path win32_useraccount where name^='ExamUserNoSpellCheck' get sid /value ^| find /i "SID"') DO SET ExamUserNoSpellCheck=%%t

FOR /F "tokens=1,2 delims==" %%s IN ('wmic path win32_useraccount where name^='ExamUserSpellCheck' get sid /value ^| find /i "SID"') DO SET ExamUserSpellCheck=%%t

:: copy the machine group policy, this calls the shutdown script to clean the machine
echo copying the computer group policy...
copy "C:\examscripts\createusers\gpt.ini_computer" "%WINDIR%\System32\GroupPolicy\gpt.ini" /Y
copy "C:\examscripts\createusers\Registry.pol_computer" "%WINDIR%\System32\GroupPolicy\Machine\Registry.pol" /Y
if exist  "%WINDIR%\System32\GroupPolicy\Machine\Scripts\psscripts.ini" ATTRIB -h "%WINDIR%\System32\GroupPolicy\Machine\Scripts\psscripts.ini"
if exist "%WINDIR%\System32\GroupPolicy\Machine\Scripts\scripts.ini" ATTRIB -h "%WINDIR%\System32\GroupPolicy\Machine\Scripts\scripts.ini"
copy "C:\examscripts\createusers\psscripts.ini_computer" "%WINDIR%\System32\GroupPolicy\Machine\Scripts\psscripts.ini" /Y
copy "C:\examscripts\createusers\scripts.ini_computer" "%WINDIR%\System32\GroupPolicy\Machine\Scripts\scripts.ini" /Y
ATTRIB +h "%WINDIR%\System32\GroupPolicy\Machine\Scripts\psscripts.ini"
ATTRIB +h "%WINDIR%\System32\GroupPolicy\Machine\Scripts\scripts.ini"
echo.

::copy the GrouPolicyUsers policies
xcopy "C:\examscripts\createusers\GroupPolicyUsers" "%WINDIR%\System32\GroupPolicyUsers\*" /E /H /Y

:: rename the group policy folders to match the SIDs of the two new accounts
rename "%WINDIR%\System32\GroupPolicyUsers\ExamUserNoSpellCheck\" %ExamUserNoSpellCheck%
rename "%WINDIR%\System32\GroupPolicyUsers\ExamUserSpellCheck\" %ExamUserSpellCheck%

::apply permissions to new group policy folders:
::the parent GroupPolicyUsers folder
icacls "C:\Windows\System32\GroupPolicyUsers" /reset /T
icacls "C:\Windows\System32\GroupPolicyUsers"  /inheritance:d 
icacls "C:\Windows\System32\GroupPolicyUsers" /grant "NT AUTHORITY\Authenticated Users":(RX)  /grant "NT AUTHORITY\Authenticated Users":(OI)(CI)(IO)(GR,GE) /grant Administrators:(OI)(CI)(IO)(F) /grant Administrators:(F) /Grant SYSTEM:F /grant SYSTEM:(OI)(CI)(IO)(F) /Q /T /C 

:: the ExamUserNoSpellCheck
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserNoSpellCheck%" /reset /T
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserNoSpellCheck%"  /inheritance:d /remove "Authenticated Users" /T /Q /C
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserNoSpellCheck%" /grant ExamUserNoSpellCheck:(RX) /grant ExamUserNoSpellCheck:(OI)(CI)(IO)(GR,GE) /grant Administrators:(F) /grant Administrators:(OI)(CI)(IO)F /Grant SYSTEM:(F) /Grant SYSTEM:(OI)(CI)(IO)F  /Q /T /C
::next the ExamUserSpellCheck
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserSpellCheck%" /reset /T
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserSpellCheck%"  /inheritance:d /remove "Authenticated Users" /T /Q /C
icacls "%WINDIR%\System32\GroupPolicyUsers\%ExamUserSpellCheck%" /grant ExamUserSpellCheck:(RX) /grant ExamUserSpellCheck:(OI)(CI)(IO)(GR,GE) /grant Administrators:(F) /grant Administrators:(OI)(CI)(IO)F /Grant SYSTEM:(F) /Grant SYSTEM:(OI)(CI)(IO)F  /Q /T /C

::make the new policy folders hidden
ATTRIB +H "%WINDIR%\System32\GroupPolicyUsers\%ExamUserNoSpellCheck%" /S /D
ATTRIB +H "%WINDIR%\System32\GroupPolicyUsers\%ExamUserSpellCheck%" /S /D
::
:: ensure the check directory exists and assign permissions
if not exist "c:\examscripts\deleteuserscheck" md "c:\examscripts\deleteuserscheck"
icacls "c:\examscripts\deleteuserscheck"  /grant ExamUserSpellCheck:(F) /grant ExamUserSpellCheck:(OI)(CI)(IO)F    /grant ExamUserNoSpellCheck:(F) /grant ExamUserNoSpellCheck:(OI)(CI)(IO)F  /grant Administrators:(F) /grant Administrators:(OI)(CI)(IO)F /Grant SYSTEM:(F) /Grant SYSTEM:(OI)(CI)(IO)F  /Q /T /C

::recreate the check file so this script does not run again on startup
fsutil file createnew "c:\examscripts\deleteuserscheck\%computername%" 1
:: assign user rights to check dir and file
icacls "c:\examscripts\deleteuserscheck"  /grant ExamUserSpellCheck:(F) /grant ExamUserSpellCheck:(OI)(CI)(IO)F    /grant ExamUserNoSpellCheck:(F) /grant ExamUserNoSpellCheck:(OI)(CI)(IO)F  /grant Administrators:(F) /grant Administrators:(OI)(CI)(IO)F /Grant SYSTEM:(F) /Grant SYSTEM:(OI)(CI)(IO)F  /Q /T /C

exit /b

:execute
::create the base directory for scripts and files

call :DELETE>"C:\examscripts\DELETEUSERS.log"  

goto end

:end
