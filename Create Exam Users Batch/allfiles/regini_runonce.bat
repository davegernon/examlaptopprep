@echo off
::Apply write permissions to the runonce registry key so that ExamUsers can initiate a system startup script to clean their data.
regini "%CD%\regini_runonce.txt"