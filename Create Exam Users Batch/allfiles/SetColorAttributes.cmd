:: SetColorAttributes.cmd 
:: Frank P. Westlake, 2006, 2007
::
@Echo OFF
SetLocal
Call :Exists Colors.exe&&(Set "PGM=Colors.exe")||(
  Call :Exists ConSetAttr.exe&&(Set "PGM=ConSetAttr.exe")||(
    Call :Exists ConSet.exe&&(Set "PGM=ConSet.exe")||(
      Call :Wrap Colors.exe or ConSetAttr.exe is required for this ^
program and none are currently in your path.
      Goto :EOF
    )
  )
)
Set "Machine=HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control"
Set "Machine=%Machine%\Session Manager\Environment"
Set "SetReg=/f /t REG_SZ"
     If /I "%1"=="/D" (Goto :Demo) ^
Else If /I "%1"=="/M" (
  Set "Reg ADD SetReg=%Machine% %SetReg%") ^
Else If /I "%1"=="/U" (
  Set "Reg ADD SetReg=HKCU\Environment %SetReg%") ^
Else If /I "%1"=="/V" (
  Set "Reg ADD SetReg=HKCU\Volatile Environment %SetReg%") ^
Else If    "%1"=="/?" (Goto :HELP) ^
Else                  (Set "SetReg=")

     If /I "%PGM%" equ "Colors.exe" (%PGM% /env=Original >NUL:) ^
Else If /I "%PGM%" equ "ConSetAttr.exe" (
                   FOR /F %%a in ('%PGM%') Do Set Original=%%a) ^
Else If /I "%PGM%" equ "ConSet.exe"     (
    %PGM% /s
    FOR /F %%a in ('%PGM% /v ConSet_Color') Do Set Original=%%a) ^
Else (Echo.Uh oh.)

Set "attrBG=%Original:~0,1%"
Set "attrFG=%Original:~1,1%"
Set /a attrB=0x%attrBG%

If DEFINED SetReg (
  "%SetReg%" /v ConsoleTextColor /d "Enabled by %~f0%"
  "%SetReg%" /v ConsoleTextColor.Highlight  /d "%attrFG%%attrBG%"
) 2>NUL: 1>&2
:: The following selects foreground colors which are suitable to the
:: current background color. These colors were chosen during a cursory
:: examination by the author and should be altered as each console
:: user desires. The values are simply the foreground color for each
:: background color 0 through F.
::
::
( EndLocal
  Set "ConsoleTextColor=Enabled by %~f0"
  Set ConsoleTextColor.Highlight=%attrFG%%attrBG%
  Set "ConsoleTextColor.Original=%Original%"
  For %%a in (
             "ConsoleTextColor.Normal=7777777007000000"
          "ConsoleTextColor.Container=DEE199991D991199"
               "ConsoleTextColor.Bold=FFFFFFF2FFFFBF66"
              "ConsoleTextColor.Error=CC44CCCCCCCCDCCC"
            "ConsoleTextColor.Caption=BBBBBBB3BB83EB43"
               "ConsoleTextColor.Soft=6668662863333367"
            "ConsoleTextColor.Enabled=AAAAAAAAAA2AAAAA"
           "ConsoleTextColor.Disabled=CCCCCCCCCCCC4CCC"
  ) Do Call :WriteVars %%~a %attrBG% "%SetReg%" >NUL:
)

Call :Demo

:: Always restore the original colors before exiting.
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Original%
Goto :EOF

:WriteVars
SetLocal
Set "FG=%2"
CALL Set FG=%%FG:~%3,1%%
Reg ADD %4 /f /t REG_SZ /v %1 /d %3%FG% 2>NUL:
EndLocal&Set %1=%3%FG%
Goto :EOF

:Demo
SetLocal
Colors /env >NUL:
For /F "tokens=1* delims==" %%a in ('Set ConsoleTextColor') Do (
  If "%%a" NEQ "ConsoleTextColor" (
    Colors %%b
    Call Echo.%%a=^%%%%a^%%
    Colors %ConsoleTextColor.Normal%
  )
)
Colors %Colors% 
If DEFINED ConsoleTextColor (
  Echo ConsoleTextColor=%ConsoleTextColor%) ^
Else (Echo ConsoleTextColor is NOT defined.)
EndLocal
Goto :EOF

::::::::::
:HELP
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Soft%
Call :Wrap Written by Frank P. Westlake, 2007-10-11
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Normal%
Call :Wrap Sets console color environment.
Echo.
Set "Wrap.Indent=  "
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Bold%
Call :Wrap %0 [/D] [/M or /U or /V]
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Normal%
Set "Wrap.Indent="
Echo.
Set "Wrap.Indent= "
Set "Wrap.Hang=    "
Call :Wrap /D Display current values. Do not set anything.
Call :Wrap /M Additionally set values in the machine's environment.
Call :Wrap /U Additionally set values in the user's environment.
Call :Wrap /V Additionally set values in the user's volatile^
 environment.
Set "Wrap.Indent="
Echo.
::Call :Wrap Color values are chosen from an internal list which^
 should be altered to the user's preferences. These values will be^
 set into the environment and may be used by programs to display^
 certain types of information in the user's color preference. For^
 example, a feature can be displayed in GREEN if it is enabled or^
 RED if it is not.
Set "Wrap.Indent="
Set "Wrap.Hang="
Call :Wrap Color values are chosen from an internal list which^
 should be altered to the user's preferences. These values will be^
 set into the environment and may be used by programs to display^
 certain types of information in the user's color preference. For^
 example, a feature can be displayed in
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Enabled%
Set/p<NUL: =  GREEN 
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Normal%
Call :Wrap if it is enabled or 
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Disabled%
Set/p<NUL: =  RED   
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Normal%
Call :Wrap if it is not.
Call :Wrap The following two instructions make those^
 changes:
Set "Wrap.Indent=  "
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Bold%
Echo.
Set "Wrap=If DEFINED ConsoleTextColor Colors"
Set "Wrap=%Wrap% %%ConsoleTextColor.Enabled%%"
Call :Wrap
Set "Wrap=If DEFINED ConsoleTextColor Colors"
Set "Wrap=%Wrap% %%ConsoleTextColor.Disabled%%"
Call :Wrap
Set "Wrap="
Echo.
Set "Wrap.Indent="
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Normal%
Call :Wrap To change colors within a line you will need a program^
 that can print without linefeeds.
Echo.
Call :Wrap Color printing may be disabled by deleting the variable^
 `ConsoleTextColor':
Echo.
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Bold%
Echo.  SET "ConsoleTextColor="
If DEFINED ConsoleTextColor Colors %ConsoleTextColor.Normal%
Goto :EOF
::::::::::

::::::::::::::::::::::::::::::::::::::::::::::::::Wrap
::Wrap
Reads text on the command line and performs word wrap printing to STDOUT.

Do NOT use tab characters. A tab is a single character when determining
line length but it could be up to eight columns when printing; which
might destroy the wordwrap feature for that line.

Input Variables:
Wrap         Read if there is no text on the command line. Use this
             variable instead of the command line when leading spaces
             or special characters need to be preserved. Escape
             special characters with '^'. For example:

               Call :Wrap Pipe COMMAND1 to COMMAND2 using the
               syntax:
                 SET "Wrap=  COMMAND1 ^| COMMAND2"
                 Call :Wrap

Wrap.Indent  Set this variable with the amount of spaces necessary
             to indent each paragraph. Indent is first line only. For
             example:

               Set "Wrap.Indent=  "
               Call :Wrap Two leading spaces.

Wrap.Hang    Set this variable with amount of spaces necessary to
             indent the paragraph after the first line. For Example:

               Set "Wrap.Hang=  "
               Call :Wrap Paragraph hang indented two spaces.

:Wrap
SetLocal
For /F "tokens=2" %%a in ('MODE^|Find "Columns"') Do (
  Set /A Cols=%%a -2)
If "%*" NEQ "" Set "Wrap=%*"
If NOT DEFINED Wrap (
  For /F "delims=" %%W in ('MORE') Do Call :Wrap %%W
  Goto :EOF
)
Set "Wrap=%Wrap.Indent%%Wrap%"
:Wrap.Loop
CALL Set "prt=%%Wrap:~0,%cols%%%"
CALL Set "Test=%%Wrap:~%cols%%%"
Set /A i=cols
:Adjust
Set /A i=i-1
If NOT DEFINED TEST Goto :Print
If "%prt:~-1,1%" NEQ " " (
  Set "prt=%prt:~0,-1%"
  Goto :Adjust
)
Set /A i=i+1
:Print
CALL Set "Wrap=%%Wrap:~%i%%%"
Echo.%prt%
If DEFINED Wrap (
  Set "Wrap=%Wrap.Hang%%Wrap%"
  Goto :Wrap.Loop
)
EndLocal
Goto :EOF
::::::::::

::::::::::
:Exists
CD>NUL:
If "%~$PATH:1"=="" DIR:>NUL: 2>&1
Goto :EOF
::::::::::
